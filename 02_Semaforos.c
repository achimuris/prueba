/*
 * Los siguientes threads cooperan para calcular el valor N2 que es la suma de 
 * los primeros N números impares. Los procesos comparten las variables N y N2 
 * inicializadas de la siguiente manera: N = 50 y N2 = 0.   
 * 
//thread 1
thread {
  while ( N > 0)
  N = N -1;
  print ( N2 );
}

//thread 2
thread
  while (true)
  N2 = N2 + 2* N + 1;

 */

#include <stdio.h>
#include <stdlib.h>
#include <semaphore.h>
#include <pthread.h>


static int numeroN = 50;
static int numero1 = 50;
static int numero2 = 0;

sem_t semaforo1;


void *restar_n1();
void *sumar_n2();

int main(){
    pthread_t h1; 
    pthread_t h2;

    sem_init(&semaforo1, 0, 1);
    

    pthread_create(&h2, NULL, *sumar_n2, NULL);
    pthread_create(&h1, NULL, *restar_n1, NULL);
    
    pthread_join(h1, NULL);
    pthread_join(h2, NULL);
    
    sem_destroy(&semaforo1); 
    
    
    return 0;

}

void *restar_n1(){

    while(numero1 > 0){
	sem_wait(&semaforo1);
        
    	numero1 = numero1 - 1;
    	// PRINTF MAGICO -> printf("\x1b[36mFuncion1 - Numero2 = %d \n\x1b[0m",numero2); // color celeste 
        printf("\x1b[36mFuncion1 - Numero2 = %d \n\x1b[0m",numero2); // color celeste 
    	sem_post(&semaforo1);	
    }	
    printf("\x1b[32mFuncion1 - Numero2 = %d (fuera del while)\n\x1b[0m",numero2); // color verde
}

void *sumar_n2(){
    int i=0;
    while(i < numeroN){
        sem_wait(&semaforo1);
        numero2 = numero2 + (2 * numero1) + 1;
        printf("\x1b[35mFuncion2 - Numero1 = %d - Numero2 = %d \n\x1b[0m",numero1,numero2); // color violeta
        sem_post(&semaforo1);
        i++;
    }
}